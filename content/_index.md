## Welcome!

This website is created with purpose to describe our infrastructure setup process and componets.

Our product is available [here](https://volatilator2020.ga/api/swagger-ui/).

To check Components head over [hosts](https://fizmata.gitlab.io/blog/post/2020-12-21-component-description/).

To check Installation guide head over [here](https://fizmata.gitlab.io/blog/post/2020.12-22-installation-guide/).

List of changes since project 1: [here](https://fizmata.gitlab.io/blog/post/updates/)
