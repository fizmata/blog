---
title: Component Description
date: 2020-12-21
---

**Table of Contents**
- [Component Diagram](#component-diagram)
- [Component Description](#component-description)
  - [Building and Deploying](#building-and-deploying)
    - [gitlab-ci](#gitlab-ci)
  - [Product](#product)
    - [General](#general)
    - [Docker and Docker-compose](#docker-and-docker-compose)
    - [Nginx Container Service](#nginx-container-service)
    - [JDK Container Service](#jdk-container-service)
    - [DNS](#dns)
  - [Monitoring](#monitoring)
    - [General](#general-1)
    - [Container-exporter Container Service](#container-exporter-container-service)
    - [Spring Actuator](#spring-actuator)
    - [Prometheus](#prometheus)
    - [Grafana](#grafana)
    - [Nginx](#nginx)
    - [Screenshot](#screenshot)

# Component Diagram
![Component diagram](/component_diagram.png)

# Component Description
## Building and Deploying
### gitlab-ci
For building and deploying our product, we use giltab-ci, it is configured to test and deploy the main branch, everytime changes are pushed to main repository. gitlab-runner builds the backend Jar file. Configuration for backend is stored separatly as `custom.yaml` inside `api-config` directory
## Product
### General
To make our product available to customers, we use the following services
* docker
* docker-compose
* nginx
* jdk
### Docker and Docker-compose
We run 3 services inside our docker container: **nginx, jdk, container-exporter**
### Nginx Container Service
Our Nginx service is configured to use https, and to pass requests to JDK service. 
### JDK Container Service
This is place where our API is located.
### DNS
All the DNS job is done by Route 53 

## Monitoring
### General
We understand, that having monitoring on the same server can create multiple issues, but since our startup did not receive enough funding, this is all we can afford.

To monitor our product, we use the following services
* container-exporter
* spring actuator
* prometheus
* grafana-server
* nginx
### Container-exporter Container Service
This third container service is built with reason of monitoring status of other containers. All the metrics are forwarded to **Prometheus**
### Spring Actuator
Inside our API container we have spring actuator set up, it exposes all endpoints to **Prometheus**
### Prometheus
Prometheus collects metrics from different exporters.
### Grafana
Grafana is used to visualize metrics, It accepts prometheus as a datasource.
### Nginx
To make **Prometheus** and **Grafana** available for developers we use Nginx. Nginx forwards requests from `/prometheus` directory to **Prometheus** and from `/grafa` directroy it forwards services to **Grafana** 

**Since port 88 and 443 are already used by our API, we use port 1860 for monitoring (it also supports https)**
### Screenshot
![Grafana screenshot](/grafana_screenshot.png)