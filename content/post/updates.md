#### List of changes since project 1

- Removed empty test - `VolatilityControllerMockMvcCacheTest.java` 

- Improved unittests to test for all `null`s and rounding

- Refactored utilities to address these edge cases
